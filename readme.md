# All Terrain Links

Cross-platform application for managing your links.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Author

Joan Botella Vinaches <https://joanbotella.com>
